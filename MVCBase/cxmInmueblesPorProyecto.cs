//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class cxmInmueblesPorProyecto
    {
        public int idInmueblePorProyecto { get; set; }
        public Nullable<int> idFranja { get; set; }
        public string catCca { get; set; }
        public Nullable<decimal> factorArea { get; set; }
        public Nullable<decimal> socioEconomico { get; set; }
        public Nullable<decimal> accesoAjuste { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public Nullable<int> idFactorUso { get; set; }
    
        public virtual cxmFranja cxmFranja { get; set; }
        public virtual FactorUso FactorUso { get; set; }
    }
}
