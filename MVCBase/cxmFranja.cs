//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class cxmFranja
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public cxmFranja()
        {
            this.cxmInmueblesPorProyecto = new HashSet<cxmInmueblesPorProyecto>();
        }
    
        public int idFranja { get; set; }
        public Nullable<int> idProyecto { get; set; }
        public Nullable<int> numFranja { get; set; }
        public Nullable<int> porcentaje { get; set; }
        public Nullable<bool> esDirecto { get; set; }
    
        public virtual cxmProyectos cxmProyectos { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<cxmInmueblesPorProyecto> cxmInmueblesPorProyecto { get; set; }
    }
}
