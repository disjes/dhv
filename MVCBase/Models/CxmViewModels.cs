﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBase.Models
{
    public class CuentasPorFranjaViewModel
    {
        public int IdInmueblePorProyecto { get; set; }
        public int? NumFranja { get; set; }
        public string CatCca { get; set; }
        public decimal? AreaTerreno { get; set; }        
        public double FactorDistancia { get; set; }
        public decimal? FactorArea { get; set; }
        public double FactorUso { get; set; }
        public decimal? SocioEconomico { get; set; }
        public decimal? AccesoAjuste { get; set; }
        public decimal FactorIndividual { get; set; }
        public decimal AreaGravable { get; set; }
        public double FactorConversion { get; set; }
        public double Gravamen { get; set; }

        public string FactorIndividualStr { get; set; }
        public string AreaGravableStr { get; set; }
        public string FactorConversionStr { get; set; }
        public string GravamenStr { get; set; }

        public virtual cxmFranja CxmFranja { get; set; }
    }

    public class FranjaViewModel
    {
        public int IdFranja { get; set; }
        public int IdProyecto { get; set; }
        public int NumFranja { get; set; }
        public int Porcentaje { get; set; }
        public bool EsDirecto { get; set; }

        public ICollection<CuentasPorFranjaViewModel> cxmInmueblesPorProyecto { get; set; }

    }

}