﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVCBase
{
    public class EncuestaViewModel
    {
        public int IdEncuesta { get; set; }
        public int IdEncuestaRespuesta { get; set; }
        public int IdNominado { get; set; }
        public bool Check { get; set; }
    }
}