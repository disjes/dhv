﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVCBase
{
    public class NominadoViewModel
    {
        public int IdNominado { get; set; }
        [Required]
        public bool Check { get; set; }
        public string Nombre { get; set; }
        public string Empresa { get; set; }
        public string Puesto { get; set; }
        public int Years { get; set; }
    }
}