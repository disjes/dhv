﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class ExpedientesController : Controller
    {
        private MesaTESTEntities db = new MesaTESTEntities();

        // GET: Expedientes
        public async Task<ActionResult> Index()
        {
            var expedientes = db.Expedientes.Include(e => e.Empleado);
            return View(await expedientes.ToListAsync());
        }

        // GET: Expedientes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Expedientes expedientes = await db.Expedientes.FindAsync(id);
            if (expedientes == null)
            {
                return HttpNotFound();
            }
            return View(expedientes);
        }

        // GET: Expedientes/Create
        public ActionResult Create()
        {
            ViewBag.idEmpleado = new SelectList(db.Empleado, "idEmpleado", "nombreEmpleado");
            return View();
        }

        // POST: Expedientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idExpediente,descripcionExpediente,idEmpleado")] Expedientes expedientes)
        {
            if (ModelState.IsValid)
            {
                db.Expedientes.Add(expedientes);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idEmpleado = new SelectList(db.Empleado, "idEmpleado", "nombreEmpleado", expedientes.idEmpleado);
            return View(expedientes);
        }

        // GET: Expedientes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Expedientes expedientes = await db.Expedientes.FindAsync(id);
            if (expedientes == null)
            {
                return HttpNotFound();
            }
            ViewBag.idEmpleado = new SelectList(db.Empleado, "idEmpleado", "nombreEmpleado", expedientes.idEmpleado);
            return View(expedientes);
        }

        // POST: Expedientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idExpediente,descripcionExpediente,idEmpleado")] Expedientes expedientes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(expedientes).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idEmpleado = new SelectList(db.Empleado, "idEmpleado", "nombreEmpleado", expedientes.idEmpleado);
            return View(expedientes);
        }

        // GET: Expedientes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Expedientes expedientes = await db.Expedientes.FindAsync(id);
            if (expedientes == null)
            {
                return HttpNotFound();
            }
            return View(expedientes);
        }

        // POST: Expedientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Expedientes expedientes = await db.Expedientes.FindAsync(id);
            db.Expedientes.Remove(expedientes);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
