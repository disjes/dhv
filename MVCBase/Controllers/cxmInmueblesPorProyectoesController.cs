﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class cxmInmueblesPorProyectoesController : Controller
    {
        private MODERNIZACION_DBEntities db = new MODERNIZACION_DBEntities();

        // GET: cxmInmueblesPorProyectoes
        public ActionResult Index()
        {
            var cxmInmueblesPorProyecto = db.cxmInmueblesPorProyecto.Include(c => c.cxmFranja).Include(c => c.FactorUso);
            return View(cxmInmueblesPorProyecto.ToList());
        }

        // GET: cxmInmueblesPorProyectoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmInmueblesPorProyecto cxmInmueblesPorProyecto = db.cxmInmueblesPorProyecto.Find(id);
            if (cxmInmueblesPorProyecto == null)
            {
                return HttpNotFound();
            }
            return View(cxmInmueblesPorProyecto);
        }

        // GET: cxmInmueblesPorProyectoes/Create
        public ActionResult Create()
        {
            ViewBag.idFranja = new SelectList(db.cxmFranja, "idFranja", "idFranja");
            ViewBag.idFactorUso = new SelectList(db.FactorUso, "idFactorUso", "factorUso1");
            return View();
        }

        // POST: cxmInmueblesPorProyectoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idInmueblePorProyecto,idFranja,catCca,factorArea,socioEconomico,accesoAjuste,fecha,idFactorUso")] cxmInmueblesPorProyecto cxmInmueblesPorProyecto)
        {
            if (ModelState.IsValid)
            {
                db.cxmInmueblesPorProyecto.Add(cxmInmueblesPorProyecto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idFranja = new SelectList(db.cxmFranja, "idFranja", "idFranja", cxmInmueblesPorProyecto.idFranja);
            ViewBag.idFactorUso = new SelectList(db.FactorUso, "idFactorUso", "factorUso1", cxmInmueblesPorProyecto.idFactorUso);
            return View(cxmInmueblesPorProyecto);
        }

        // GET: cxmInmueblesPorProyectoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmInmueblesPorProyecto cxmInmueblesPorProyecto = db.cxmInmueblesPorProyecto.Find(id);
            if (cxmInmueblesPorProyecto == null)
            {
                return HttpNotFound();
            }
            ViewBag.idFranja = new SelectList(db.cxmFranja, "idFranja", "idFranja", cxmInmueblesPorProyecto.idFranja);
            ViewBag.idFactorUso = new SelectList(db.FactorUso, "idFactorUso", "factorUso1", cxmInmueblesPorProyecto.idFactorUso);
            return View(cxmInmueblesPorProyecto);
        }

        // POST: cxmInmueblesPorProyectoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idInmueblePorProyecto,idFranja,catCca,factorArea,socioEconomico,accesoAjuste,fecha,idFactorUso")] cxmInmueblesPorProyecto cxmInmueblesPorProyecto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cxmInmueblesPorProyecto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idFranja = new SelectList(db.cxmFranja, "idFranja", "idFranja", cxmInmueblesPorProyecto.idFranja);
            ViewBag.idFactorUso = new SelectList(db.FactorUso, "idFactorUso", "factorUso1", cxmInmueblesPorProyecto.idFactorUso);
            return View(cxmInmueblesPorProyecto);
        }

        // GET: cxmInmueblesPorProyectoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmInmueblesPorProyecto cxmInmueblesPorProyecto = db.cxmInmueblesPorProyecto.Find(id);
            if (cxmInmueblesPorProyecto == null)
            {
                return HttpNotFound();
            }
            return View(cxmInmueblesPorProyecto);
        }

        // POST: cxmInmueblesPorProyectoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            cxmInmueblesPorProyecto cxmInmueblesPorProyecto = db.cxmInmueblesPorProyecto.Find(id);
            db.cxmInmueblesPorProyecto.Remove(cxmInmueblesPorProyecto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
