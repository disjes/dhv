﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class CuentasCorrientesController : Controller
    {
        private MODERNIZACION_DBEntities db = new MODERNIZACION_DBEntities();

        // GET: CuentasCorrientes
        public ActionResult Index()
        {
            return View(db.CuentasCorrientes.ToList());
        }

        // GET: CuentasCorrientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CuentasCorrientes cuentasCorrientes = db.CuentasCorrientes.Find(id);
            if (cuentasCorrientes == null)
            {
                return HttpNotFound();
            }
            return View(cuentasCorrientes);
        }

        public ActionResult ProjectSummary(int idCuenta)
        {
            var ccPorProyecto = db.CuentasCorrientesPorProyecto.FirstOrDefault(x => x.idCuentaCorriente == idCuenta);
            var inmueble = db.consultaInmuebles.FirstOrDefault(x => x.CatCca == ccPorProyecto.CuentasCorrientes.catCca);
            ViewBag.inmueble = inmueble;
            ViewBag.contribuyente = db.consultaContribuyente.FirstOrDefault(x => x.RicCod == inmueble.RicCod);
            return View(ccPorProyecto);
        }

        [HttpPost]
        public ActionResult PerformSubmit(string addPrima, string accountState, string addPago, int idCuentaCorriente)
        {
            if (addPrima != null) return RedirectToAction("Create", "Pagos", new { idCuentaCorriente = idCuentaCorriente, isPrima = true });
            if (accountState != null) return RedirectToAction("Index");
            if (addPago != null) return RedirectToAction("Create", "Pagos", new { idCuentaCorriente = idCuentaCorriente, isPrima = false });
            return RedirectToAction("Index");
        }

        // GET: CuentasCorrientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CuentasCorrientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idCuenta,catCca,tipoCuenta,montoAdeudado,fecha")] CuentasCorrientes cuentasCorrientes)
        {
            if (ModelState.IsValid)
            {
                db.CuentasCorrientes.Add(cuentasCorrientes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cuentasCorrientes);
        }

        // GET: CuentasCorrientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CuentasCorrientes cuentasCorrientes = db.CuentasCorrientes.Find(id);
            if (cuentasCorrientes == null)
            {
                return HttpNotFound();
            }
            return View(cuentasCorrientes);
        }

        // POST: CuentasCorrientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idCuenta,catCca,tipoCuenta,montoAdeudado,fecha")] CuentasCorrientes cuentasCorrientes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cuentasCorrientes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cuentasCorrientes);
        }

        // GET: CuentasCorrientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CuentasCorrientes cuentasCorrientes = db.CuentasCorrientes.Find(id);
            if (cuentasCorrientes == null)
            {
                return HttpNotFound();
            }
            return View(cuentasCorrientes);
        }

        // POST: CuentasCorrientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CuentasCorrientes cuentasCorrientes = db.CuentasCorrientes.Find(id);
            db.CuentasCorrientes.Remove(cuentasCorrientes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
