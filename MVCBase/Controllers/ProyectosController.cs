﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using MVCBase;
using MVCBase.Models;

namespace MVCBase.Controllers
{
    public class ProyectosController : Controller
    {
        private MODERNIZACION_DBEntities db = new MODERNIZACION_DBEntities();

        // GET: Proyectos
        public ActionResult Index()
        {
            return View(db.cxmProyectos.ToList());
        }

        // GET: Proyectos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmProyectos cxmProyectos = db.cxmProyectos.Find(id);
            if (cxmProyectos == null)
            {
                return HttpNotFound();
            }
            return View(cxmProyectos);
        }

        // GET: Proyectos/Details/5
        public ActionResult Preview(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmProyectos proy = db.cxmProyectos.Find(id);                  
            if (proy == null)
            {
                return HttpNotFound();
            }
            List<CuentasPorFranjaViewModel> cuentas = new List<CuentasPorFranjaViewModel>();
            foreach (var cxmFranja in proy.cxmFranja)
            {
                foreach (var inmueble in cxmFranja.cxmInmueblesPorProyecto)
                    cuentas.Add(PerformCalculos(CreateCuenta(inmueble), proy));
            }
            ViewBag.cuentas = cuentas;
            this.ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return View(proy);
        }

        private CuentasPorFranjaViewModel CreateCuenta(cxmInmueblesPorProyecto inmueble)
        {
            return new CuentasPorFranjaViewModel()
            {
                IdInmueblePorProyecto = inmueble.idInmueblePorProyecto,
                NumFranja = inmueble.cxmFranja.numFranja,
                CatCca = inmueble.catCca,
                AreaTerreno = db.consultaInmuebles.FirstOrDefault(x => x.CatCca == inmueble.catCca)?.CatAre,
                FactorDistancia = 0.0,
                FactorArea = inmueble.factorArea,
                FactorUso = 0.0,
                SocioEconomico = inmueble.socioEconomico,
                AccesoAjuste = inmueble.accesoAjuste
            };            
        }

        private CuentasPorFranjaViewModel PerformCalculos(CuentasPorFranjaViewModel cuentasPorFranja, cxmProyectos proyecto)
        {
            string validations = validateFields(cuentasPorFranja);
            if (validations == "Ok")
            {
                cuentasPorFranja.FactorIndividual = Convert.ToDecimal((
                    Convert.ToDouble(cuentasPorFranja.AreaTerreno)*
                    cuentasPorFranja.FactorDistancia*
                    Convert.ToDouble(cuentasPorFranja.FactorArea)*
                    cuentasPorFranja.FactorUso*
                    Convert.ToDouble(cuentasPorFranja.SocioEconomico)*
                    Convert.ToDouble(cuentasPorFranja.AccesoAjuste)
                    ));
                cuentasPorFranja.AreaGravable = cuentasPorFranja.FactorIndividual*(decimal) cuentasPorFranja.AreaTerreno;
                cuentasPorFranja.FactorConversion = Convert.ToDouble((cuentasPorFranja.AreaGravable/
                                                                      ((proyecto.areaEjecutada)/100)));
                cuentasPorFranja.Gravamen = Convert.ToDouble(cuentasPorFranja.AreaGravable)*
                                            cuentasPorFranja.FactorConversion;
                setValuesToStrs(cuentasPorFranja);
            } else setErrorToFields(cuentasPorFranja, validations);
            return cuentasPorFranja;            
        }

        private void setValuesToStrs(CuentasPorFranjaViewModel cuentasPorFranja)
        {
            cuentasPorFranja.FactorIndividualStr = cuentasPorFranja.FactorIndividual.ToString();
            cuentasPorFranja.AreaGravableStr = cuentasPorFranja.AreaGravable.ToString();
            cuentasPorFranja.FactorConversionStr = cuentasPorFranja.FactorConversion.ToString();
            cuentasPorFranja.GravamenStr = cuentasPorFranja.Gravamen.ToString();
        }

        private void setErrorToFields(CuentasPorFranjaViewModel cuentasPorFranja, string message)
        {
            cuentasPorFranja.FactorIndividualStr = message;
            cuentasPorFranja.AreaGravableStr = message;
            cuentasPorFranja.FactorConversionStr = message;
            cuentasPorFranja.GravamenStr = message;
        }

        private string validateFields(CuentasPorFranjaViewModel cuentasPorFranja)
        {
            if (cuentasPorFranja.AreaTerreno == null) return "Revisar Área Terreno";
            if (cuentasPorFranja.FactorDistancia == null || Math.Abs(cuentasPorFranja.FactorDistancia) < 0.1)
                return "Revisar Factor Distancia";
            if (cuentasPorFranja.FactorArea == null) return "Revisar Factor Área";
            if (cuentasPorFranja.FactorUso == null || Math.Abs(cuentasPorFranja.FactorUso) < 0.1)
                return "Revisar Factor Uso";
            if (cuentasPorFranja.SocioEconomico == null) return "Revisar Factor SocioEconómico";
            if (cuentasPorFranja.AccesoAjuste == null) return "Revisar Acceso Ajuste";
            return "Ok";
        }

        public string getLastTicket()
        {
            return "'<p>CCCCC0587</p>'";
        }
        // GET: Proyectos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Proyectos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idProyecto,cxmFranja,cxmInmueblesPorProyecto,nombreProyecto,ubicacion,descripcion,contratista,valorProyecto,montoContratado,montoEjecutado,areaContratada,areaEjecutada,medidaArea,fechaInicio,fechaFinalizacion,cantidadFranjas")] cxmProyectos cxmProyectos)
        {
            if (ModelState.IsValid)
            {
                db.cxmProyectos.Add(cxmProyectos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cxmProyectos);
        }

        // GET: Proyectos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmProyectos cxmProyectos = db.cxmProyectos.Find(id);
            if (cxmProyectos == null)
            {
                return HttpNotFound();
            }
            return View(cxmProyectos);
        }

        // POST: Proyectos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idProyecto,nombreProyecto,ubicacion,descripcion,contratista,valorProyecto,montoContratado,montoEjecutado,areaContratada,areaEjecutada,medidaArea,fechaInicio,fechaFinalizacion,cantidadFranjas")] cxmProyectos cxmProyectos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cxmProyectos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cxmProyectos);
        }        

        [HttpPost]
        public ActionResult PerformSubmit(string addCca, string closeCarga, string printDocs, string createCc, int idProyecto)
        {   
            if (addCca != null) return RedirectToAction("Preview");
            if (closeCarga != null) return Index();
            if (printDocs != null) return Index();
            if (createCc != null) return CrearCuentasCorrientes(idProyecto);
            return RedirectToAction("Index");
        }

        // GET: Proyectos/Cuenta/5
        public ActionResult CrearCuentasCorrientes(int idProyecto)
        {
            var proyecto = db.cxmProyectos.FirstOrDefault(x => x.idProyecto == idProyecto);
            var tipoCuenta = 1;
            foreach (var franja in proyecto.cxmFranja)
            {
                foreach (var inmueble in franja.cxmInmueblesPorProyecto)
                {
                    var ccPorProyecto = new CuentasCorrientesPorProyecto()
                    {
                        idCuentaCorriente = 0,                        
                        idProyecto = idProyecto,
                        CuentasCorrientes = new CuentasCorrientes()
                        {
                            idCuenta = 0,
                            catCca = inmueble.catCca,
                            tipoCuenta = tipoCuenta,
                            montoAdeudado = proyecto.areaEjecutada,
                            fecha = DateTime.Now
                            // cuentasPorFranja.FactorIndividual * (decimal)cuentasPorFranja.AreaTerreno                
                        }
                    };
                    db.CuentasCorrientesPorProyecto.Add(ccPorProyecto);
                }                                                                                
            }
            db.SaveChanges();
            return RedirectToAction("Index", "CuentasCorrientes");
        }

        // GET: Proyectos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmProyectos cxmProyectos = db.cxmProyectos.Find(id);
            if (cxmProyectos == null)
            {
                return HttpNotFound();
            }
            return View(cxmProyectos);
        }

        // POST: Proyectos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            cxmProyectos cxmProyectos = db.cxmProyectos.Find(id);
            db.cxmProyectos.Remove(cxmProyectos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
