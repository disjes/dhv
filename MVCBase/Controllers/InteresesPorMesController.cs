﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class InteresesPorMesController : Controller
    {
        private MODERNIZACION_DBEntities db = new MODERNIZACION_DBEntities();

        // GET: InteresesPorMes
        public ActionResult Index()
        {
            return View(db.InteresesPorMes.ToList());
        }

        // GET: InteresesPorMes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InteresesPorMes interesesPorMes = db.InteresesPorMes.Find(id);
            if (interesesPorMes == null)
            {
                return HttpNotFound();
            }
            return View(interesesPorMes);
        }

        // GET: InteresesPorMes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: InteresesPorMes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idInteres,mes,tasa,anio")] InteresesPorMes interesesPorMes)
        {
            if (ModelState.IsValid)
            {
                db.InteresesPorMes.Add(interesesPorMes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(interesesPorMes);
        }

        // GET: InteresesPorMes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InteresesPorMes interesesPorMes = db.InteresesPorMes.Find(id);
            if (interesesPorMes == null)
            {
                return HttpNotFound();
            }
            return View(interesesPorMes);
        }

        // POST: InteresesPorMes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idInteres,mes,tasa,anio")] InteresesPorMes interesesPorMes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(interesesPorMes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(interesesPorMes);
        }

        // GET: InteresesPorMes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InteresesPorMes interesesPorMes = db.InteresesPorMes.Find(id);
            if (interesesPorMes == null)
            {
                return HttpNotFound();
            }
            return View(interesesPorMes);
        }

        // POST: InteresesPorMes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InteresesPorMes interesesPorMes = db.InteresesPorMes.Find(id);
            db.InteresesPorMes.Remove(interesesPorMes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
