//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class consultaInmuebles
    {
        public Nullable<int> RicCod { get; set; }
        public int CatCod { get; set; }
        public string CatCca { get; set; }
        public string CatMap { get; set; }
        public string CatBlq { get; set; }
        public string CatPre { get; set; }
        public Nullable<int> CarFic { get; set; }
        public string DepCod { get; set; }
        public string DepDes { get; set; }
        public string MunCod { get; set; }
        public string MunDes { get; set; }
        public string AldCod { get; set; }
        public string AldDes { get; set; }
        public string CDPCod { get; set; }
        public string CDPDes { get; set; }
        public Nullable<int> NatCod { get; set; }
        public string NatDes { get; set; }
        public string DocMunCod { get; set; }
        public string DocDes { get; set; }
        public Nullable<decimal> CatAre { get; set; }
        public string UArCod { get; set; }
        public string UArDes { get; set; }
        public string TMeCod { get; set; }
        public string TMeDes { get; set; }
        public string CatFAd { get; set; }
        public Nullable<int> CatMon { get; set; }
        public string CTrCod { get; set; }
        public string CTrDes { get; set; }
        public string CatMaq { get; set; }
        public Nullable<int> ETrCod { get; set; }
        public string ETrDes { get; set; }
        public Nullable<int> TPrCod { get; set; }
        public string TPrDes { get; set; }
        public string CatAnt { get; set; }
        public Nullable<int> UsoCod { get; set; }
        public string UsoDes { get; set; }
        public Nullable<int> SUsCod { get; set; }
        public string SUsDes { get; set; }
        public Nullable<int> CHaCod { get; set; }
        public string CHaDes { get; set; }
        public Nullable<int> PSuCod { get; set; }
        public string PSuDes { get; set; }
        public string RPrCod { get; set; }
        public string RPrDes { get; set; }
        public string CatTom { get; set; }
        public string CatFol { get; set; }
        public string CatAsi { get; set; }
        public string CatFln { get; set; }
        public Nullable<int> ColCod { get; set; }
        public string ColdDes { get; set; }
        public string CatDecJur { get; set; }
    }
}
